<?php
if (!defined('IN_FINECMS')) exit();

/**
 * 管理后台菜单项
 */

return array(

	'index' => array(
		'name' => 'home',
		'icon' => 'home',
		'uri' => 'admin/index/main',
		'menu' => array(
		),
	),
	'system' => array(
		'name' => 'system-config',
		'icon' => 'cog',
		'menu' => array(
			array(
				'name' => 'system-config',
				'icon' => 'cog',
				'uri' => 'admin/index/config',
			),
			array(
				'name' => 'app-name',
				'icon' => 'trademark',
				'uri' => 'admin/index/bq',
			),
			array(
				'name' => 'dr004',
				'icon' => 'envelope',
				'uri' => 'admin/sms/index',
			),
			array(
				'name' => 'administrator',
				'icon' => 'user',
				'uri' => 'admin/user/index',
			),
			array(
				'name' => 'a-aut-13',
				'icon' => 'users',
				'uri' => 'admin/auth/index',
			),
		),
	),
	'syslog' => array(
		'name' => 'system-security',
		'icon' => 'shield',
		'menu' => array(
			array(
				'name' => 'a-ind-43',
				'icon' => 'calendar',
				'uri' => 'admin/index/log',
			),
			array(
				'name' => 'a-men-66',
				'icon' => 'calendar-times-o',
				'uri' => 'admin/index/attack',
			),
			array(
				'name' => 'a-men-67',
				'icon' => 'close',
				'uri' => 'admin/ip/index',
			),
		),
	),
	'site' => array(
		'name' => 'a-men-72',
		'icon' => 'cubes',
		'menu' => array(
			array(
				'name' => 'a-men-72',
				'icon' => 'cubes',
				'uri' => 'admin/site/index',
			),
			array(
				'name' => 'a-men-26',
				'icon' => 'navicon',
				'uri' => 'admin/category/index',
			),
			array(
				'name' => 'm-con-5',
				'icon' => 'folder',
				'uri' => 'admin/attachment/index',
			),
			array(
				'name' => 'a-men-25',
				'icon' => 'database',
				'url' => url('admin/model/index', array('typeid' => 1)),
			),
			array(
				'name' => 'a-men-60',
				'icon' => 'database',
				'url' => url('admin/model/index', array('typeid' => 3)),
			),
			array(
				'name' => 'a-men-30',
				'icon' => 'file-text',
				'uri' => 'admin/block/index',
			),
			array(
				'name' => 'push-area',
				'icon' => 'flag',
				'uri' => 'admin/position/index',
			),
			array(
				'name' => 'a-men-32',
				'icon' => 'tag',
				'uri' => 'admin/tag/index',
			),
			array(
				'name' => 'content-chain',
				'icon' => 'retweet',
				'uri' => 'admin/relatedlink/index',
			),
			array(
				'name' => 'a-mod-91',
				'icon' => 'pie-chart',
				'uri' => 'admin/linkage/index',
			),
		),
	),
	'content' => array(
		'name' => 'a-men-29',
		'icon' => 'television',
		'menu' => array(
			array('test'),
		),
	),
	'html' => array(
		'name' => 'content-generation',
		'icon' => 'refresh',
		'menu' => array(
			array(
				'name' => 'update-the-url',
				'icon' => 'refresh',
				'uri' => 'admin/content/updateurl',
			),
			array(
				'name' => 'a-men-50',
				'icon' => 'refresh',
				'uri' => 'admin/html/index',
			),
			array(
				'name' => 'generate-sitemap',
				'icon' => 'sitemap',
				'uri' => 'admin/index/updatemap',
			),
		),
	),
	'weixin' => array(
		'name' => 'wechat-public',
		'icon' => 'weixin',
		'menu' => array(
			array('name' => lang('dr019'), 'url' => url('admin/wx/config'),    'icon' => 'cog'),
			array('name' => lang('dr020'), 'url' => url('admin/wx/index'),    'icon' => 'weixin'),
			array('name' => lang('dr021'), 'url' => url('admin/wx/keyword'),    'icon' => 'tag'),
			array('name' => lang('dr023'), 'url' => url('admin/wx/menu'),    'icon' => 'table'),
			array('name' => lang('dr024'), 'url' => url('admin/wx/user'),    'icon' => 'user'),
		)
	),
	'theme' => array(
		'name' => 'a-men-36',
		'icon' => 'code',
		'uri' => 'admin/theme/index',
	),
	'member' => array(
		'name' => 'a-men-36',
		'icon' => 'user',
		'menu' => array(
			array(
				'name' => 'a-men-36',
				'icon' => 'user',
				'uri' => 'admin/member/index',
			),
			array(
				'name' => 'a-mem-57',
				'icon' => 'envelope',
				'uri' => 'admin/member/pms',
			),
			array(
				'name' => 'a-mem-30',
				'icon' => 'users',
				'uri' => 'admin/member/group',
			),
			array(
				'name' => 'a-mem-29',
				'icon' => 'database',
				'url' => url('admin/model/index', array('typeid' => 2)),
			),
			array(
				'name' => 'member-extension-model',
				'icon' => 'database',
				'url' => url('admin/model/index', array('typeid' => 4)),
			),
			array(
				'name' => 'member-parameter-configuration',
				'icon' => 'cog',
				'uri' => 'admin/member/config',
			),
		),
	),
	'plugin' => array(
		'name' => 'plug-in-management',
		'icon' => 'cloud',
		'menu' => array(
			array(
				'name' => 'plug-in-management',
				'icon' => 'cloud',
				'uri' => 'admin/plugin/index',
			),
		),
	),

);

/*
 * FineCMS语言文件 for edit_area后台模板编辑器
 * 注意：若更改了语言目录名称，下面的editAreaLoader.lang["zh-cn"]也要更改对应目录名称（zh-cn）
 */
editAreaLoader.lang["en-us"] = {
    new_document: "Create a new blank document",
    search_button: "Find and Replace",
    search_command: "Find Next / Opens the Find box",
    search: "Find",
    replace: "Replace",
    replace_command: "Replace / open the search box",
    find_next: "Find the next one",
    replace_all: "Replace all",
    reg_exp: "Regular Expressions",
    match_case: "Match case",
    not_found: "Not found.",
    occurrence_replaced: "Is replaced.",
    search_field_empty: "The Find box has no content",
    restart_search_at_begin: "Reached the end of the document.",
    move_popup: "Move the Find dialog box",
    font_size: "--font size--",
    go_to_line: "Go to the row",
    go_to_line_prompt: "Go to line::",
    undo: "Restore",
    redo: "Redo",
    change_smooth_selection: "Enable / disable some display features (better looking but more resource-intensive)",
    highlight: "Enables / disables syntax highlighting",
    reset_highlight: "Reset syntax highlighting (when the text display is not synchronized)",
    word_wrap: "Toggle word wrapping mode",
    help: "About",
    save: "Save",
    load: "Load",
    line_abbr: "Row",
    char_abbr: "Character",
    position: "Position",
    total: "Total",
    close_popup: "Close the dialog box",
    shortcuts: "Hot key",
    add_tab: "Add a Tab (Tab)",
    remove_tab: "Remove tab (Tab)",
    about_notice: "Note: Syntax highlighting is used only for text with less content (too much content will cause the browser to respond slowly)",
    toggle: "Toggle the editor",
    accesskey: "Hot key",
    tab: "Tab",
    shift: "Shift",
    ctrl: "Ctrl",
    esc: "Esc",
    processing: "Processing ...",
    fullscreen: "Full screen editing",
    syntax_selection: "--grammar--",
    close_tab: "Close the text"
};
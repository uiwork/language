/*
 * FineCMS语言文件 for calendar日期时间字段
 * 注意：若更改了语言目录名称，下面的Calendar.LANG("zh-cn", ... 也要更改对应目录名称（zh-cn）
 */
Calendar.LANG("en-us", "English", {
    fdow: 1, // first day of week for this locale; 0 = Sunday, 1 = Monday, etc.
    goToday: "Today",
    today: "Today", // appears in bottom bar
    wk: "Week",
    weekend: "0,6", // 0 = Sunday, 1 = Monday, etc.
    AM: "AM",
    PM: "PM",
    mn: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    smn: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    dn: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
    sdn: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
});



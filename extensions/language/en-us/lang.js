/*
 * FineCMS语言文件 for js
 */
var fc_lang = new Array();
fc_lang[0] = 'No image';
fc_lang[1] = 'Preview';
fc_lang[2] = 'Remove';
fc_lang[3] = 'Upload';
fc_lang[4] = 'Sort';
fc_lang[5] = 'Mouse drag';
fc_lang[6] = 'Determine';
fc_lang[7] = 'Cancel';
fc_lang[8] = 'Uploading Please wait ...';
fc_lang[9] = 'Uploading';
fc_lang[10] = 'please wait...';
fc_lang[11] = 'File uploaded successfully';
fc_lang[12] = 'Upload error';
fc_lang[13] = 'Server I / O error';
fc_lang[14] = 'Server security authentication error';
fc_lang[15] = 'Attachment security check failed, upload terminated';
fc_lang[16] = 'Upload canceled';
fc_lang[17] = 'Upload terminated';
fc_lang[18] = 'The number of uploads per file is limited to';
fc_lang[19] = 'Please do not upload empty files';
fc_lang[20] = 'Queue file number exceeds the set value';
fc_lang[21] = 'The file size exceeds the set value';
fc_lang[22] = 'File type is invalid';
fc_lang[23] = 'Upload error, please contact the administrator!';
fc_lang[24] = 'You have not uploaded';
fc_lang[25] = 'File information';
fc_lang[26] = 'No document';